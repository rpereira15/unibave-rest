package br.com.rpereira15.rest.service;

import br.com.rpereira15.rest.model.Produto;
import br.com.rpereira15.rest.repository.ProdutoRepository;

import java.util.Optional;
import javax.inject.Inject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProdutoServiceImpl implements ProdutoService {

    @Inject
    private ProdutoRepository repository;

    @Override
    @Transactional
    public Produto adiciona(Produto novo) {
        return repository.save(novo);
    }

    @Override
    @Transactional
    public Produto atualiza(Long codigo, Produto produto) {
        if (!codigo.equals(produto.getCodigo())) {
            throw new IllegalArgumentException("Código inválido");
        }
        return repository.save(produto);
    }

    @Override
    public Optional<Produto> busca(Long codigo) {
        return Optional.ofNullable(repository.findOne(codigo));
    }

    @Override
    @Transactional
    public void deleta(final Long codigo) {
        busca(codigo).ifPresent(repository::delete);
    }

    @Override
    public Page<Produto> lista(Pageable pageable, String nome) {
        return nome != null
                ? repository.findByNomeContaining(pageable, nome)
                : repository.findAll(pageable);
    }

}
