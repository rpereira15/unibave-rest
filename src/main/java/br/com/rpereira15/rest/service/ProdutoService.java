package br.com.rpereira15.rest.service;


import java.util.Optional;

import br.com.rpereira15.rest.model.Produto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProdutoService {

    Produto adiciona(Produto novo);


    Produto atualiza(Long codigo, Produto produto);

    /**
     * Busca o Produto
     * 
     * @param codigo Código do produto
     * @return Produto se existir
     */
    Optional<Produto> busca(Long codigo);

    void deleta(Long codigo);

    Page<Produto> lista(Pageable pageable, String nome);

}
