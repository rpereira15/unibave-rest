package br.com.rpereira15.rest.model;

import lombok.Data;

@Data
public class ProdutoDTO {

    private Long codigo;
    
    private String nome;
    
    private String descricao;

    private Integer quantidade;

    private Double preco;

    public ProdutoDTO(Produto produto) {
        this.codigo = produto.getCodigo();
        this.nome = produto.getNome();
        this.preco = produto.getPreco();
        this.quantidade = produto.getQuantidade();
    }
    
}
