package br.com.rpereira15.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnibaveRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnibaveRestApplication.class, args);
	}
}
