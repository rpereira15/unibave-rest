package br.com.rpereira15.rest.resource;

import br.com.rpereira15.rest.model.Produto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api(value = "Cadastro de Alunos", tags = "produtos",
        consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
@Path("/produtos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ProdutoResource {

    @ApiOperation(value = "Adiciona produto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Produto adicionado com sucesso", response = Produto.class)
    })
    @POST
    Response adiciona(@Valid final Produto produto);

    @ApiOperation(value = "Atualiza produto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Produto atualizado com sucesso", response = Produto.class)
        ,
        @ApiResponse(code = 404, message = "Produto não encontrado")
    })
    @PUT
    @Path(value = "{codigo}")
    Response atualiza(@PathParam(value = "codigo") final Long codigo, @Valid final Produto produto);

    @ApiOperation(value = "Deleta produto")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Produto deletado com sucesso")
        ,
        @ApiResponse(code = 404, message = "Produto não encontrado")
    })
    @DELETE
    @Path(value = "{codigo}")
    Response deleta(@PathParam(value = "codigo") final Long codigo);

    @ApiOperation(value = "Busca produto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Produto retornado com sucesso", response = Produto.class)
        ,
        @ApiResponse(code = 404, message = "Produto não encontrado")
    })
    @GET
    @Path(value = "{codigo}")
    Response busca(@PathParam(value = "codigo") final Long codigo);

    @ApiOperation(value = "Lista produto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Lista de produtos", response = Produto.class, responseContainer = "List")
    })
    @GET
    Response lista(@QueryParam(value = "nome") final String nome,
            @QueryParam(value = "page") @DefaultValue("0") final int page,
            @QueryParam(value = "limit") @DefaultValue("10") final int limit,
            @QueryParam(value = "sort") @DefaultValue("codigo") final String sort,
            @QueryParam(value = "direction") @DefaultValue("asc") final String direction);
}
