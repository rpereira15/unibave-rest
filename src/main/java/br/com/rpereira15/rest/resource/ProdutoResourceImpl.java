package br.com.rpereira15.rest.resource;

import br.com.rpereira15.rest.model.ProdutoDTO;
import br.com.rpereira15.rest.model.Produto;
import br.com.rpereira15.rest.service.ProdutoService;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class ProdutoResourceImpl implements ProdutoResource {
 
    @Inject
    private ProdutoService service;

    @Override
    public Response adiciona(Produto novo) {
        return Response.ok(service.adiciona(novo)).build();
    }

    @Override
    public Response atualiza(Long codigo,
                             Produto produto) {
        return Response.ok(service.atualiza(codigo, produto)).build();
    }

    @Override
    public Response busca(Long codigo) {
        Produto produto = service.busca(codigo)
                .orElseThrow(() -> new NotFoundException());
        return Response.ok(new ProdutoDTO(produto)).build();
    }

    @Override
    public Response deleta(Long codigo) {
        service.deleta(codigo);
        return Response.noContent().build();
    }

    @Override
    public Response lista(String nome, int page, int limit, String sort, String direction) {
        Pageable pageagle = new PageRequest(page, limit, 
                Sort.Direction.fromString(direction), sort);
        return Response.ok(service.lista(pageagle, nome)).build();
    }
}
