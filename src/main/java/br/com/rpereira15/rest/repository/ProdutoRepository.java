package br.com.rpereira15.rest.repository;


import br.com.rpereira15.rest.model.Produto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdutoRepository
        extends PagingAndSortingRepository<Produto, Long> {
    
    Page<Produto> findByNomeContaining(Pageable pageable, String nome);
}
